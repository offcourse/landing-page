(ns landing-page.steps
  (:require [landing-page.section :refer [Section]]
            [landing-page.step :refer [Step]]))

(defn Steps [{:keys [section-type steps]}]
  [Section {:type section-type}
   [:div.ui.container
   [:div.ui.three.column.stackable.grid
    (for [{:keys [title] :as step-data} steps]
      ^{:key title} [Step step-data])]]])
