(ns landing-page.splash
  (:require [landing-page.steps :refer [Steps]]
            [landing-page.logo :refer [Logo]]
            [landing-page.sign-up-form :refer [SignUpForm]]
            [landing-page.section :refer [Section]]))

(defn Splash [{:keys [section-type slogan steps content]}]
  [Section {:type section-type
            :size :large
            :padded true}
   [:div.ui.container
    [:div.ui.two.column.left.aligned.stackable.grid
     [:div.ui.ten.wide.column
      [:h1.ui.huge.header slogan]
      [:p.lead content]]
     [:div.ui.six.wide.column
      [:div.ui.padded.segment
       [:h1.ui.header "Sign up"]
       [:p "Create an account on Offcourse and start collecting and sharing what you know right away."]
       [:a.ui.expanded.button.bottompadding {:href "https://offcourse.io/sign-up"} "Create account"]
       [:p.topmargin
        [:a {:href "https://offcourse.io/platform"} "Or browse the open platform."]]]]]]])