(ns landing-page.sign-up-form)
(defn SignUpForm []
  [:form.ui.form {:action "https://offcourse.us13.list-manage.com/subscribe/post" :method "POST"}
   [:input {:type "hidden" :name "u" :value "3888907a37a3a88f5d6d88573"}]
   [:input {:type "hidden" :name "id" :value "200371cb67"}]
   [:div {:class "field"}
    [:label "Email"]
    [:input {:type "email"
             :autocapitalize "off"
             :autocorrect "off"
             :name "MERGE0"
             :id "MERGE0"
             :placeholder "Email address"}]]
   [:input.ui.expanded.button {:type "submit" :name "submit" :value "Join mailing list"}]])
